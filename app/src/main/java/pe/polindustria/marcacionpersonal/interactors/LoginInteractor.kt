package pe.polindustria.marcacionpersonal.interactors

import android.util.Log
import android.widget.Toast
import pe.polindustria.marcacionpersonal.interfaces.Login
import pe.polindustria.marcacionpersonal.interfaces.PolindustriaService
import pe.polindustria.marcacionpersonal.models.LoginObject
import pe.polindustria.marcacionpersonal.models.MyResponse
import pe.polindustria.marcacionpersonal.models.User
import pe.polindustria.marcacionpersonal.presenters.LoginPresenter
import pe.polindustria.marcacionpersonal.utils.MyRetrofit
import retrofit2.Call
import retrofit2.Response
import java.util.logging.Handler
import javax.security.auth.callback.Callback

class LoginInteractor : Login.LoginInteractor {
    var presenter: LoginPresenter? = null

    constructor(loginPresenter: LoginPresenter) {
        this.presenter = loginPresenter
    }

    override fun validate(username: String?, password: String?) {

        if (username!!.length == 0 || password!!.length == 0) {

                presenter!!.error();
        } else {
            submit(username,password)
        }


    }

    override fun submit(username: String?, password: String?) {

        var service = MyRetrofit.mRetrofit.create(PolindustriaService::class.java);
        this.presenter!!.showProgress()
        service.login(LoginObject(username,password)).enqueue(object : retrofit2.Callback<MyResponse<User>> {
            override fun onFailure(call: Call<MyResponse<User>>, t: Throwable) {
                presenter!!.hideProgress()
                presenter!!.error()
                Log.e("RETROFIT error",t.message)
            }

            override fun onResponse(call: Call<MyResponse<User>>, response: Response<MyResponse<User>>) {
                presenter!!.hideProgress()
                if (response.isSuccessful){
                    var body = response.body();
                    if (body!!.status == "success"){
                        presenter!!.succcess(body.data.name)
                        setPreferences(body.data)
                        presenter!!.goToMain()
                    }else if(body.status == "warning"){
                        presenter!!.error()
                    }


                }
            }

        })
    }

    override fun setPreferences(user:User) {
        presenter!!.setPreferences(user)
    }

}