package pe.polindustria.marcacionpersonal.interactors;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import pe.polindustria.marcacionpersonal.interfaces.Personal;
import pe.polindustria.marcacionpersonal.interfaces.PolindustriaService;
import pe.polindustria.marcacionpersonal.models.MyResponse;
import pe.polindustria.marcacionpersonal.presenters.PersonalPresenter;
import pe.polindustria.marcacionpersonal.utils.MyRetrofit;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class PersonalInteractor implements Personal.Interactor {

    PersonalPresenter presenter;
    PolindustriaService retrofit;

    public PersonalInteractor(PersonalPresenter presenter) {
        this.presenter = presenter;
    }


    @Override
    public void getList() {
        List<pe.polindustria.marcacionpersonal.models.Personal> data = new ArrayList<>();
        presenter.showLoading();
        retrofit = MyRetrofit.mRetrofit.create(PolindustriaService.class);
        retrofit.getPersonal().enqueue(new Callback<MyResponse<List<pe.polindustria.marcacionpersonal.models.Personal>>>() {
            @Override
            public void onResponse(Call<MyResponse<List<pe.polindustria.marcacionpersonal.models.Personal>>> call, Response<MyResponse<List<pe.polindustria.marcacionpersonal.models.Personal>>> response) {
                data.addAll(response.body().getData());
                presenter.showData(data);
                presenter.hideLoading();
            }

            @Override
            public void onFailure(Call<MyResponse<List<pe.polindustria.marcacionpersonal.models.Personal>>> call, Throwable t) {
                Log.e("ERROR RETROFIT",t.getMessage());
                presenter.showErrorLoadData();
                presenter.hideLoading();
            }
        });

    }
}
