package pe.polindustria.marcacionpersonal.utils

import android.content.Context
import android.content.DialogInterface
import com.google.android.material.dialog.MaterialAlertDialogBuilder

object CustomDialog {
    fun messageDialog(context: Context?, title: String?, message: String?, cancelable: Boolean = true): MaterialAlertDialogBuilder {
        var dialog = MaterialAlertDialogBuilder(context)
        if (title != "") {
            dialog.setTitle(title)
        }
        if (message != "") {
            dialog.setMessage(message)
        }
        if (cancelable != true) {
            dialog.setCancelable(cancelable)
        }
        return dialog
    }

    fun confirmDialog(context: Context, message: String,callbackPositive:DialogInterface.OnClickListener,callbackNegative:DialogInterface.OnClickListener): MaterialAlertDialogBuilder {
        var dialog = MaterialAlertDialogBuilder(context);

        dialog.setMessage(message)
        dialog.setPositiveButton("Si",callbackPositive)
        dialog.setNegativeButton("No",callbackNegative)


        return dialog;
    }

}