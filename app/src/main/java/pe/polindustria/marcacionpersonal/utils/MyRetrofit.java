package pe.polindustria.marcacionpersonal.utils;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MyRetrofit {

    public static Retrofit mRetrofit = new Retrofit.Builder()
            .baseUrl("http://192.168.1.2:8000/api/v2/")
            .addConverterFactory(GsonConverterFactory.create())
            .build();


}
