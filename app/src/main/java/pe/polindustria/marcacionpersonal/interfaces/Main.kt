package pe.polindustria.marcacionpersonal.interfaces

import android.view.MenuItem
import android.widget.FrameLayout
import android.widget.Toolbar
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import com.google.android.material.navigation.NavigationView

interface Main {
    interface MainInteractor {

    }

    interface MainPresenter {

    }

    interface MainView {
        fun setToolbar();
        fun setItemsNav();
        fun changeFragment(content: FrameLayout, fragment: Fragment, item: MenuItem);
        fun defaultFragment();
        fun setNameHeader(navigationView: NavigationView);
        fun closeSession();
        fun closeDrawer(drawerLayout: DrawerLayout);
        fun changeTitleToolbar(toolbar: androidx.appcompat.widget.Toolbar,title:String);
    }
}