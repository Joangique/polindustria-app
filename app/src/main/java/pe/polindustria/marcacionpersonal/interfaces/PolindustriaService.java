package pe.polindustria.marcacionpersonal.interfaces;

import java.util.List;

import pe.polindustria.marcacionpersonal.models.LoginObject;
import pe.polindustria.marcacionpersonal.models.MyResponse;
import pe.polindustria.marcacionpersonal.models.Personal;
import pe.polindustria.marcacionpersonal.models.User;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface PolindustriaService {

    @POST("login")
    Call<MyResponse<User>> login(@Body LoginObject login);

    @GET("personal")
    Call<MyResponse<List<Personal>>> getPersonal();

}
