package pe.polindustria.marcacionpersonal.interfaces;

import java.util.List;

public interface Personal {

    interface Interactor {
        void getList();
    }

    interface Presenter {
        void getList();
        void requestData();
        void showData(List<pe.polindustria.marcacionpersonal.models.Personal> data);
        void showErrorLoadData();
        void showLoading();
        void hideLoading();
    }

    interface View {
        void showData(List<pe.polindustria.marcacionpersonal.models.Personal> data);
        void requestData();
        void showErrorLoadData();
        void showLoading();
        void hideLoading();
    }

}
