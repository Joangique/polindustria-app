package pe.polindustria.marcacionpersonal.interfaces

import pe.polindustria.marcacionpersonal.models.User

interface Login {
    interface LoginInteractor {
        fun validate(username: String?, password: String?)
        fun submit(username: String?, password: String?)
        fun setPreferences(user:User)
    }

    interface LoginPresenter {
        fun showProgress()
        fun hideProgress()
        fun succcess(username: String);
        fun error();
        fun onLogin(username: String?, password: String?)
        fun validate(username: String?, password: String?)
        fun goToMain();
        fun setPreferences(user:User)
    }

    interface LoginView {
        fun showProgress()
        fun hideProgress()
        fun succcess(username: String);
        fun error();
        fun onLogin(username: String?, password: String?)
        fun goToMain();
        fun saveUser(user:User);
    }
}