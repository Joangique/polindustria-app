package pe.polindustria.marcacionpersonal.customviews;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class MontSerratTextView extends androidx.appcompat.widget.AppCompatTextView {

    public MontSerratTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public MontSerratTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MontSerratTextView(Context context) {
        super(context);
        init();
    }

    private void init() {
        Typeface tf = Typeface.createFromAsset(super.getContext().getAssets(),
                "fonts/montserrat.ttf");
        setTypeface(tf);
    }

}
