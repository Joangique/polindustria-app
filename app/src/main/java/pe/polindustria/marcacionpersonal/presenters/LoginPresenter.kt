package pe.polindustria.marcacionpersonal.presenters

import pe.polindustria.marcacionpersonal.interactors.LoginInteractor
import pe.polindustria.marcacionpersonal.interfaces.Login
import pe.polindustria.marcacionpersonal.models.User
import pe.polindustria.marcacionpersonal.views.LoginView

class LoginPresenter : Login.LoginPresenter {

    var view: Login.LoginView? = null;
    var interactor: Login.LoginInteractor? = null;

    constructor(view:LoginView) {
        this.view = view;
        this.interactor = LoginInteractor(this);
    }


    override fun showProgress() {
        view!!.showProgress();
    }

    override fun hideProgress() {
        view!!.hideProgress();
    }


    override fun succcess(username: String) {
        view!!.succcess(username);
    }

    override fun error() {
        view!!.error();
    }

    override fun onLogin(username: String?, password: String?) {
        interactor!!.validate(username,password);
    }


    override fun validate(username: String?, password: String?) {

    }

    override fun goToMain() {
        view!!.goToMain();
    }

    override fun setPreferences(user: User) {
        view!!.saveUser(user);
    }

}