package pe.polindustria.marcacionpersonal.presenters;

import android.view.View;

import java.util.List;

import pe.polindustria.marcacionpersonal.interactors.PersonalInteractor;
import pe.polindustria.marcacionpersonal.interfaces.Personal;
import pe.polindustria.marcacionpersonal.views.PersonalView;

public class PersonalPresenter implements Personal.Presenter {


    Personal.View view;
    Personal.Interactor interactor;

    public PersonalPresenter(PersonalView view) {
        this.view = view;
        this.interactor = new PersonalInteractor(this);
    }
    @Override
    public void getList() {
        interactor.getList();
    }

    @Override
    public void requestData() {
        interactor.getList();
    }

    @Override
    public void showData(List<pe.polindustria.marcacionpersonal.models.Personal> data) {
        view.showData(data);
    }

    @Override
    public void showErrorLoadData() {
        view.showErrorLoadData();
    }

    @Override
    public void showLoading() {
        view.showLoading();
    }

    @Override
    public void hideLoading() {
        view.hideLoading();
    }


}
