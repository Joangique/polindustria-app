package pe.polindustria.marcacionpersonal.views

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.Toast
import androidx.annotation.Nullable
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import pe.polindustria.marcacionpersonal.R
import pe.polindustria.marcacionpersonal.adapters.PersonalAdapter
import pe.polindustria.marcacionpersonal.interfaces.Personal
import pe.polindustria.marcacionpersonal.presenters.PersonalPresenter

/**
 * A simple [Fragment] subclass.
 */
class PersonalView : Fragment(), Personal.View {
    lateinit var presenter:PersonalPresenter
    lateinit var rv :RecyclerView;
    lateinit var pb :ProgressBar;
    private lateinit var viewAdapter: RecyclerView.Adapter<*>
    private lateinit var viewManager: RecyclerView.LayoutManager
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val view = inflater.inflate(R.layout.fragment_personal, container, false)

        rv = view.findViewById(R.id.rv_PersonalFragment);
        pb = view.findViewById(R.id.pb_PersonalFragment);

        presenter = PersonalPresenter(this);
        requestData()

        return view!!
    }

    override fun showData(data: List<pe.polindustria.marcacionpersonal.models.Personal>?) {
        Toast.makeText(context,"Un total de "+data!!.size,Toast.LENGTH_SHORT).show();

        viewManager = LinearLayoutManager(context)
        viewAdapter = PersonalAdapter(context,data,R.layout.item_personal)
        rv.layoutManager = viewManager;
        rv.adapter = viewAdapter;



    }

    override fun showLoading() {
        pb.visibility= View.VISIBLE
        rv.visibility= View.GONE
    }

    override fun showErrorLoadData() {
        Toast.makeText(context,"Error al intentar conectarse con el servidor",Toast.LENGTH_SHORT).show();
    }

    override fun hideLoading() {
        pb.visibility= View.GONE
        rv.visibility= View.VISIBLE
    }

    override fun requestData() {
        presenter.getList()
    }





}
