package pe.polindustria.marcacionpersonal.views

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import pe.polindustria.marcacionpersonal.R

/**
 * A simple [Fragment] subclass.
 */
class AsistenciaView : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_asistencia, container, false);
        return view;
    }

}
