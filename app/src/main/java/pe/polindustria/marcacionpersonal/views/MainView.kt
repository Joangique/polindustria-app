package pe.polindustria.marcacionpersonal.views

import android.Manifest
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.telephony.TelephonyManager
import android.view.MenuItem
import android.widget.FrameLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.app.ActivityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import com.google.android.material.navigation.NavigationView
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.single.PermissionListener
import pe.polindustria.marcacionpersonal.R
import pe.polindustria.marcacionpersonal.interfaces.Main
import pe.polindustria.marcacionpersonal.utils.CustomDialog


class MainView : AppCompatActivity(), Main.MainView {

    lateinit var toolbar: Toolbar;
    lateinit var drawer: DrawerLayout;
    lateinit var navview: NavigationView;
    lateinit var drawerToggle: ActionBarDrawerToggle;
    lateinit var content: FrameLayout;
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        toolbar = findViewById(R.id.toolbar_Main)
        drawer = findViewById(R.id.drawer_Main)
        navview = findViewById(R.id.navigation_Main)
        content = findViewById(R.id.content_Main)
        setToolbar()
        setNameHeader(navview)
        setItemsNav()
        defaultFragment()




    }

    override fun setToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar()!!.setDisplayHomeAsUpEnabled(true);
        getSupportActionBar()!!.setHomeAsUpIndicator(R.drawable.ic_menu_white);
        drawerToggle = ActionBarDrawerToggle(this, drawer, toolbar, R.string.app_name, R.string.app_name);
        drawer.addDrawerListener(drawerToggle);
    }

    override fun setItemsNav() {
        navview.setNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.mCerrarSesion -> {
                    closeSession()
                }
                R.id.mPersonal -> {
                    changeFragment(content, PersonalView(), it)
                }
                R.id.mOTs -> {
                    changeFragment(content, OTsView(), it)
                }
                R.id.mAsistencia -> {
                    changeFragment(content, AsistenciaView(), it)
                }
            }
            true;
        }

    }

    override fun changeFragment(content: FrameLayout, fragment: Fragment, item: MenuItem) {
        val transaction = supportFragmentManager.beginTransaction();
        transaction.replace(content.id, fragment)
        transaction.commit()
        item.setChecked(true)
        changeTitleToolbar(toolbar, item.title.toString())
        closeDrawer(drawer)
    }

    override fun defaultFragment() {
        changeFragment(content, PersonalView(), navview.menu.getItem(0))
    }

    override fun setNameHeader(navigationView: NavigationView) {
        val preferences = getSharedPreferences("pe.polindustria.marcacionpersonal", Context.MODE_PRIVATE);
        val header = navigationView.getHeaderView(0)
        val tvEmail = header.findViewById<TextView>(R.id.tvemail_header)
        tvEmail.text = preferences.getString("user_email","email@email.com")
    }

    override fun closeSession() {
        val dialogConfirm = CustomDialog.confirmDialog(MainView@ this,
                "Esta realmente seguro de cerrar sesión?",
                object : DialogInterface.OnClickListener {
                    override fun onClick(dialog: DialogInterface?, which: Int) {
                        dialog!!.dismiss();

                        val preferences = getSharedPreferences("pe.polindustria.marcacionpersonal", Context.MODE_PRIVATE);

                        preferences.edit().clear().commit();

                        var intent = Intent(applicationContext, LoginView::class.java);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                        startActivity(intent)
                    }

                },
                object : DialogInterface.OnClickListener {
                    override fun onClick(dialog: DialogInterface?, which: Int) {
                        dialog!!.dismiss();
                    }

                })
        dialogConfirm.show();
    }

    override fun closeDrawer(drawerLayout: DrawerLayout) {
        drawerLayout.closeDrawers()
    }

    override fun changeTitleToolbar(toolbar: Toolbar, title: String) {
        toolbar.setTitle(title);
    }


}
