package pe.polindustria.marcacionpersonal.views

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.snackbar.Snackbar
import com.rilixtech.materialfancybutton.MaterialFancyButton
import pe.polindustria.marcacionpersonal.R
import pe.polindustria.marcacionpersonal.interfaces.Login
import pe.polindustria.marcacionpersonal.models.User
import pe.polindustria.marcacionpersonal.presenters.LoginPresenter
import pe.polindustria.marcacionpersonal.utils.CustomDialog

class LoginView : AppCompatActivity(), Login.LoginView {
    lateinit var cargando: AlertDialog;
    lateinit var etUsuario: EditText
    lateinit var etClave: EditText
    lateinit var tvMensaje: TextView
    lateinit var btnIngresar: MaterialFancyButton
    lateinit var cargandoDialog: MaterialAlertDialogBuilder

    var presenter: LoginPresenter? = null;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        etUsuario = findViewById(R.id.etUsuario_Login)
        etClave = findViewById(R.id.etClave_Login)
        btnIngresar = findViewById(R.id.btnIngresar_Login)
        tvMensaje = findViewById(R.id.tvMensaje_Login)
        presenter = LoginPresenter(this);
        cargandoDialog = CustomDialog.messageDialog(this, null, "Cargando, espere por favor.", false);


        verificaLogueado()

        btnIngresar.setOnClickListener({
            onLogin(etUsuario.text.toString(), etClave.text.toString())
        })


    }

    fun verificaLogueado() {
        var preferences = getSharedPreferences("pe.polindustria.marcacionpersonal", Context.MODE_PRIVATE);
        if (preferences.getInt("user_id", 0) != 0) {
            goToMain()
        }
    }

    override fun showProgress() {
        cargando = cargandoDialog.show();

    }

    override fun hideProgress() {
        cargando.dismiss();
    }

    override fun succcess(username: String) {
        Snackbar.make(window.decorView.rootView, "Bienvenido ${username}.", Snackbar.LENGTH_LONG).show();
    }

    override fun error() {
        tvMensaje.visibility = View.VISIBLE
        tvMensaje.setTextColor(ContextCompat.getColor(applicationContext, R.color.primary_light))
        tvMensaje.text = "Estas creedenciales no son las correctas."
        Snackbar.make(window.decorView.rootView, "Estas creedenciales no son las correctas.", Snackbar.LENGTH_LONG).show();
    }

    override fun onLogin(username: String?, password: String?) {
        this.presenter!!.onLogin(username, password);
    }

    override fun goToMain() {
        var intent = Intent(this, MainView::class.java);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
    }

    override fun saveUser(user: User) {
        var preferences = getSharedPreferences("pe.polindustria.marcacionpersonal", Context.MODE_PRIVATE);
        var edit = preferences.edit();

        edit.clear()

        edit.putInt("user_id", user.id)
        edit.putString("user_name", user.name)
        edit.putString("user_email", user.email)
        edit.putString("user_token", user.rememberToken)
        edit.commit()
    }


}