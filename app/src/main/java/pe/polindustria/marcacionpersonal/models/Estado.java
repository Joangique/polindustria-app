package pe.polindustria.marcacionpersonal.models;

public class Estado {
    Integer id;
    String titulo;
    Integer muestra;
    String created_at;
    String updated_at;

    public Estado() {
    }

    public Estado(Integer id, String titulo, Integer muestra, String created_at, String updated_at) {
        this.id = id;
        this.titulo = titulo;
        this.muestra = muestra;
        this.created_at = created_at;
        this.updated_at = updated_at;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public Integer getMuestra() {
        return muestra;
    }

    public void setMuestra(Integer muestra) {
        this.muestra = muestra;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }
}
