package pe.polindustria.marcacionpersonal.models;

public class Cargo {
    private Integer id;
    private String titulo;
    private String createdAt;
    private String updatedAt;

    public Cargo() {
    }

    public Cargo(Integer id, String titulo, String createdAt, String updatedAt) {
        this.id = id;
        this.titulo = titulo;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }
}
