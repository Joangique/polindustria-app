package pe.polindustria.marcacionpersonal.models;

public class Personal {
    private Integer id;
    private String nombres;
    private String apellidos;
    private String numdoc;
    private Integer tipdocId;
    private Integer cargoId;
    private Integer horarioId;
    private Estado estado;
    private String createdAt;
    private String updatedAt;
    private Tipdoc tipdoc;
    private Cargo cargo;
    private Horario horario;
    private String image;


    public Personal() {

    }

    public Personal(Integer id, String nombres, String apellidos, String numdoc, Integer tipdocId, Integer cargoId, Integer horarioId, Estado estado, String createdAt, String updatedAt, Tipdoc tipdoc, Cargo cargo, Horario horario, String image) {
        this.id = id;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.numdoc = numdoc;
        this.tipdocId = tipdocId;
        this.cargoId = cargoId;
        this.horarioId = horarioId;
        this.estado = estado;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.tipdoc = tipdoc;
        this.cargo = cargo;
        this.horario = horario;
        this.image = image;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getNumdoc() {
        return numdoc;
    }

    public void setNumdoc(String numdoc) {
        this.numdoc = numdoc;
    }

    public Integer getTipdocId() {
        return tipdocId;
    }

    public void setTipdocId(Integer tipdocId) {
        this.tipdocId = tipdocId;
    }

    public Integer getCargoId() {
        return cargoId;
    }

    public void setCargoId(Integer cargoId) {
        this.cargoId = cargoId;
    }

    public Integer getHorarioId() {
        return horarioId;
    }

    public void setHorarioId(Integer horarioId) {
        this.horarioId = horarioId;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Tipdoc getTipdoc() {
        return tipdoc;
    }

    public void setTipdoc(Tipdoc tipdoc) {
        this.tipdoc = tipdoc;
    }

    public Cargo getCargo() {
        return cargo;
    }

    public void setCargo(Cargo cargo) {
        this.cargo = cargo;
    }

    public Horario getHorario() {
        return horario;
    }

    public void setHorario(Horario horario) {
        this.horario = horario;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
