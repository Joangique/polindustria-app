package pe.polindustria.marcacionpersonal.adapters;

import android.app.Person;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.List;

import pe.polindustria.marcacionpersonal.R;
import pe.polindustria.marcacionpersonal.customviews.MontSerratTextView;
import pe.polindustria.marcacionpersonal.models.Personal;

public class PersonalAdapter extends RecyclerView.Adapter<PersonalAdapter.ViewHolder> {

    Context context;
    List<Personal> list;
    int layout;

    public PersonalAdapter(Context context, List<Personal> list, int layout) {
        this.context = context;
        this.list = list;
        this.layout = layout;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(layout, parent, false);
        ViewHolder vh = new ViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Personal p = list.get(position);
        holder.tvNombre.setText(p.getApellidos() + ", " + p.getNombres());
        holder.tvCargo.setText(p.getCargo().getTitulo());
        holder.tvHorario.setText(p.getHorario().getTitulo());


        Glide.with(context).load(p.getImage()).centerCrop()
                .placeholder(R.drawable.team).into(holder.iv);

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        MontSerratTextView tvNombre, tvCargo, tvHorario;
        ImageView iv;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvNombre = itemView.findViewById(R.id.tvNombre_PersonalItem);
            tvCargo = itemView.findViewById(R.id.tvCargo_PersonalItem);
            tvHorario = itemView.findViewById(R.id.tvHorario_PersonalItem);
            iv = itemView.findViewById(R.id.iv_PersonalItem);

        }
    }

}