package pe.polindustria.marcacionpersonal;

import android.app.Application;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import pe.polindustria.marcacionpersonal.models.MyResponse;

public class MyApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        Realm.init(this);
        RealmConfiguration config = new RealmConfiguration.Builder().build();
        Realm.setDefaultConfiguration(config);


    }
}
